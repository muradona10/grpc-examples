package com.md.motorsports.model;

import lombok.Data;

@Data
public class Rider {

    private Long ID;

    private String name;

    private String surname;

    private String profilePhotoUrl;

    private String countryPhotoUrl;

    private String teamName;

    private Long number;

}
