package com.md.motorsports.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Riders {

    private List<Rider> riders = new ArrayList<>();

}
