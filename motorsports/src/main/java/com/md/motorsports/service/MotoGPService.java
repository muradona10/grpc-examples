package com.md.motorsports.service;

import com.md.motorsports.model.Rider;
import com.md.motorsports.model.Riders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.ParameterizedType;
import java.util.List;

@Service
public class MotoGPService {

    @Autowired
    private RestTemplate restTemplate;

    public List<Rider> riders(){

        return restTemplate.getForObject("http://localhost:8082/motogp/rest/riders", Riders.class).getRiders();
    }

}
