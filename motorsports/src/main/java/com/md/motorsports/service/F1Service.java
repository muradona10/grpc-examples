package com.md.motorsports.service;

import com.md.formula1.gen.proto.*;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class F1Service {

    @GrpcClient("formula1-service")
    private Formula1ServiceGrpc.Formula1ServiceBlockingStub f1BlockingStub;

    public List<F1Team> f1Teams(){
        GetAllTeamsRequest getAllTeamsRequest = GetAllTeamsRequest.newBuilder().build();
        return f1BlockingStub.getAllTeams(getAllTeamsRequest).getTeamsList();
    }

    public List<F1Driver> drivers(){
        GetAllDriversRequest getAllDriversRequest = GetAllDriversRequest.newBuilder().build();
        return f1BlockingStub.getAllDrivers(getAllDriversRequest).getDriversList();
    }


}
