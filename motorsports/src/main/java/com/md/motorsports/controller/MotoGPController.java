package com.md.motorsports.controller;

import com.md.motorsports.model.Rider;
import com.md.motorsports.service.MotoGPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/motor-sports/motogp")
public class MotoGPController {

    @Autowired
    MotoGPService motoGPService;

    @GetMapping("/teams")
    public String teams(Model model){
        List<Rider> riders = motoGPService.riders();
        List<String> teams = riders.stream().map(Rider::getTeamName).distinct().collect(Collectors.toList());
        model.addAttribute("teams",teams);
        return "motogpTeams";
    }

    @GetMapping("/riders")
    public String riders(Model model){
        List<Rider> riders = motoGPService.riders();
        model.addAttribute("riders",riders);
        return "riders";
    }

}
