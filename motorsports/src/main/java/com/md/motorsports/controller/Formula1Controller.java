package com.md.motorsports.controller;

import com.md.formula1.gen.proto.F1Driver;
import com.md.formula1.gen.proto.F1Team;
import com.md.motorsports.service.F1Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/motor-sports/f1")
public class Formula1Controller {

    @Autowired
    private F1Service f1Service;

    @GetMapping("/teams")
    public String teams(Model model){
        List<F1Team> teams = f1Service.f1Teams();
        model.addAttribute("teams",teams);
        return "f1Teams";
    }

    @GetMapping("/drivers")
    public String drivers(Model model){
        List<F1Driver> drivers = f1Service.drivers();
        model.addAttribute("drivers",drivers);
        return "drivers";
    }


}
