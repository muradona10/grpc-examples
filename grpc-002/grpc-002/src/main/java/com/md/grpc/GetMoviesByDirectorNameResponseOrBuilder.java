// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: MovieLibrary.proto

package com.md.grpc;

public interface GetMoviesByDirectorNameResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.md.grpc.GetMoviesByDirectorNameResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated .com.md.grpc.Movie movies = 1;</code>
   */
  java.util.List<com.md.grpc.Movie> 
      getMoviesList();
  /**
   * <code>repeated .com.md.grpc.Movie movies = 1;</code>
   */
  com.md.grpc.Movie getMovies(int index);
  /**
   * <code>repeated .com.md.grpc.Movie movies = 1;</code>
   */
  int getMoviesCount();
  /**
   * <code>repeated .com.md.grpc.Movie movies = 1;</code>
   */
  java.util.List<? extends com.md.grpc.MovieOrBuilder> 
      getMoviesOrBuilderList();
  /**
   * <code>repeated .com.md.grpc.Movie movies = 1;</code>
   */
  com.md.grpc.MovieOrBuilder getMoviesOrBuilder(
      int index);
}
