package com.md.grpc;

import java.util.ArrayList;
import java.util.List;

import com.md.grpc.GetMoviesByDirectorNameGrpc.GetMoviesByDirectorNameImplBase;

import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class GetMoviesByDirectorNameImpl extends GetMoviesByDirectorNameImplBase {

	public void getMoviesByDirectorName(GetMoviesByDirectorNameRequest request
			                           ,StreamObserver<GetMoviesByDirectorNameResponse> responseObserver) {
		
		String directorName = request.getDirectorName();
		List<Movie> movieLibrary = initMovieLibrary();
		List<Movie> moviesByDirectorName = getMoviesByDirectorName(directorName, movieLibrary);
		
		GetMoviesByDirectorNameResponse response = GetMoviesByDirectorNameResponse.newBuilder()
				                                                                  .addAllMovies(moviesByDirectorName)
				                                                                  .build();
		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}
	
	public List<Movie> initMovieLibrary(){
		
		List<Movie> movies = new ArrayList<>();
		
		Movie.Gender gender1 = Movie.Gender.newBuilder().setId(1).setName("Comedy").build();
		Movie.Gender gender2 = Movie.Gender.newBuilder().setId(2).setName("Action").build();
		Movie.Gender gender3 = Movie.Gender.newBuilder().setId(3).setName("Sci-Fi").build();
		List<Movie.Gender> genders = new ArrayList<>();
		genders.add(gender2);
		genders.add(gender3);

		Movie.Director director1 = Movie.Director.newBuilder().setId(1).setName("Director1").setImdb("sk1022609").build();
		Movie.Director director2 = Movie.Director.newBuilder().setId(2).setName("Director2").setImdb("sk1022610").build();
		List<Movie.Director> directors = new ArrayList<>();
		directors.add(director1);
		directors.add(director2);
		
		// movie1
		Movie movie1 = Movie.newBuilder().setId(1).setTitle("Thousands Words").setYear(2009).setImdb("tt1034064")
				.addGenders(gender1).addAllDirectors(directors).build();

		// movie2
		Movie movie2 = Movie.newBuilder().setId(2).setTitle("Gamer").setYear(2009).setImdb("tt1034032")
				.addAllGenders(genders).addDirectors(director1).build();
		
		movies.add(movie1);
		movies.add(movie2);
		
		return movies;
	}
	
	public List<Movie> getMoviesByDirectorName(String directorName, List<Movie> movies) {

		List<Movie> moviesByDirectorName = new ArrayList<>();
		
		for (Movie m : movies) {
			List<Movie.Director> directors = m.getDirectorsList();
			for (Movie.Director d : directors) {
				if (d.getName().equals(directorName)) {
					moviesByDirectorName.add(m);
				}
			}

		}

		return moviesByDirectorName;
	}
	
}
