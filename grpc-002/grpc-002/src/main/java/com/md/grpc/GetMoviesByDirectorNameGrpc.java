package com.md.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.16.1)",
    comments = "Source: MovieLibrary.proto")
public final class GetMoviesByDirectorNameGrpc {

  private GetMoviesByDirectorNameGrpc() {}

  public static final String SERVICE_NAME = "com.md.grpc.GetMoviesByDirectorName";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.md.grpc.GetMoviesByDirectorNameRequest,
      com.md.grpc.GetMoviesByDirectorNameResponse> getGetMoviesByDirectorNameMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getMoviesByDirectorName",
      requestType = com.md.grpc.GetMoviesByDirectorNameRequest.class,
      responseType = com.md.grpc.GetMoviesByDirectorNameResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.md.grpc.GetMoviesByDirectorNameRequest,
      com.md.grpc.GetMoviesByDirectorNameResponse> getGetMoviesByDirectorNameMethod() {
    io.grpc.MethodDescriptor<com.md.grpc.GetMoviesByDirectorNameRequest, com.md.grpc.GetMoviesByDirectorNameResponse> getGetMoviesByDirectorNameMethod;
    if ((getGetMoviesByDirectorNameMethod = GetMoviesByDirectorNameGrpc.getGetMoviesByDirectorNameMethod) == null) {
      synchronized (GetMoviesByDirectorNameGrpc.class) {
        if ((getGetMoviesByDirectorNameMethod = GetMoviesByDirectorNameGrpc.getGetMoviesByDirectorNameMethod) == null) {
          GetMoviesByDirectorNameGrpc.getGetMoviesByDirectorNameMethod = getGetMoviesByDirectorNameMethod = 
              io.grpc.MethodDescriptor.<com.md.grpc.GetMoviesByDirectorNameRequest, com.md.grpc.GetMoviesByDirectorNameResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "com.md.grpc.GetMoviesByDirectorName", "getMoviesByDirectorName"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.md.grpc.GetMoviesByDirectorNameRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.md.grpc.GetMoviesByDirectorNameResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new GetMoviesByDirectorNameMethodDescriptorSupplier("getMoviesByDirectorName"))
                  .build();
          }
        }
     }
     return getGetMoviesByDirectorNameMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static GetMoviesByDirectorNameStub newStub(io.grpc.Channel channel) {
    return new GetMoviesByDirectorNameStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static GetMoviesByDirectorNameBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new GetMoviesByDirectorNameBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static GetMoviesByDirectorNameFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new GetMoviesByDirectorNameFutureStub(channel);
  }

  /**
   */
  public static abstract class GetMoviesByDirectorNameImplBase implements io.grpc.BindableService {

    /**
     */
    public void getMoviesByDirectorName(com.md.grpc.GetMoviesByDirectorNameRequest request,
        io.grpc.stub.StreamObserver<com.md.grpc.GetMoviesByDirectorNameResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetMoviesByDirectorNameMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetMoviesByDirectorNameMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.md.grpc.GetMoviesByDirectorNameRequest,
                com.md.grpc.GetMoviesByDirectorNameResponse>(
                  this, METHODID_GET_MOVIES_BY_DIRECTOR_NAME)))
          .build();
    }
  }

  /**
   */
  public static final class GetMoviesByDirectorNameStub extends io.grpc.stub.AbstractStub<GetMoviesByDirectorNameStub> {
    private GetMoviesByDirectorNameStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GetMoviesByDirectorNameStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GetMoviesByDirectorNameStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GetMoviesByDirectorNameStub(channel, callOptions);
    }

    /**
     */
    public void getMoviesByDirectorName(com.md.grpc.GetMoviesByDirectorNameRequest request,
        io.grpc.stub.StreamObserver<com.md.grpc.GetMoviesByDirectorNameResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetMoviesByDirectorNameMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class GetMoviesByDirectorNameBlockingStub extends io.grpc.stub.AbstractStub<GetMoviesByDirectorNameBlockingStub> {
    private GetMoviesByDirectorNameBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GetMoviesByDirectorNameBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GetMoviesByDirectorNameBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GetMoviesByDirectorNameBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.md.grpc.GetMoviesByDirectorNameResponse getMoviesByDirectorName(com.md.grpc.GetMoviesByDirectorNameRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetMoviesByDirectorNameMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class GetMoviesByDirectorNameFutureStub extends io.grpc.stub.AbstractStub<GetMoviesByDirectorNameFutureStub> {
    private GetMoviesByDirectorNameFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GetMoviesByDirectorNameFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GetMoviesByDirectorNameFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GetMoviesByDirectorNameFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.md.grpc.GetMoviesByDirectorNameResponse> getMoviesByDirectorName(
        com.md.grpc.GetMoviesByDirectorNameRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetMoviesByDirectorNameMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_MOVIES_BY_DIRECTOR_NAME = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final GetMoviesByDirectorNameImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(GetMoviesByDirectorNameImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_MOVIES_BY_DIRECTOR_NAME:
          serviceImpl.getMoviesByDirectorName((com.md.grpc.GetMoviesByDirectorNameRequest) request,
              (io.grpc.stub.StreamObserver<com.md.grpc.GetMoviesByDirectorNameResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class GetMoviesByDirectorNameBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    GetMoviesByDirectorNameBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.md.grpc.MovieLibrary.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("GetMoviesByDirectorName");
    }
  }

  private static final class GetMoviesByDirectorNameFileDescriptorSupplier
      extends GetMoviesByDirectorNameBaseDescriptorSupplier {
    GetMoviesByDirectorNameFileDescriptorSupplier() {}
  }

  private static final class GetMoviesByDirectorNameMethodDescriptorSupplier
      extends GetMoviesByDirectorNameBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    GetMoviesByDirectorNameMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (GetMoviesByDirectorNameGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new GetMoviesByDirectorNameFileDescriptorSupplier())
              .addMethod(getGetMoviesByDirectorNameMethod())
              .build();
        }
      }
    }
    return result;
  }
}
