package com.md.grpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.md")
public class Grpc002Application {

	public static void main(String[] args) {
		SpringApplication.run(Grpc002Application.class, args);
	}

}
