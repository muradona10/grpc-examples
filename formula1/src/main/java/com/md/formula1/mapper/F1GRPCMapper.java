package com.md.formula1.mapper;

import com.md.formula1.gen.proto.F1Driver;
import com.md.formula1.gen.proto.F1Team;
import com.md.formula1.model.Driver;
import com.md.formula1.model.Team;

public class F1GRPCMapper {

    public static F1Driver mapToF1Driver(Driver driver) {

        Team team = driver.getTeam();

        F1Team f1Team = F1Team.newBuilder().setId(team.getId().intValue())
                .setName(team.getName())
                .setCountryName(team.getCountryName())
                .setCountryFlagUrl(team.getCountryFlagUrl()).build();

        F1Driver f1Driver = F1Driver.newBuilder().setId(driver.getId().intValue())
                .setProfilePhotoUrl(driver.getProfilePhotoUrl())
                .setFirstName(driver.getFirstName())
                .setLastName(driver.getLastName())
                .setNumber(driver.getNumber().intValue())
                .setCountryName(driver.getCountryName())
                .setCountryFlagUrl(driver.getCountryFlagUrl())
                .setTeam(f1Team).build();

        return f1Driver;

    }

    public static F1Team mapToF1Team(Team team) {

        F1Team f1Team = F1Team.newBuilder().setId(team.getId().intValue())
                .setName(team.getName())
                .setCountryName(team.getCountryName())
                .setCountryFlagUrl(team.getCountryFlagUrl()).build();

        return f1Team;

    }

}
