package com.md.formula1.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Team")
@Table(name="f1_team")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "county_name")
    private String countryName;

    @Column(name = "county_flag_url")
    private String countryFlagUrl;

    @OneToMany(mappedBy = "team")
    private List<Driver> drivers = new ArrayList<>();

    public Team(String name, String countryName, String countryFlagUrl) {
        this.name = name;
        this.countryName = countryName;
        this.countryFlagUrl = countryFlagUrl;
    }
}
