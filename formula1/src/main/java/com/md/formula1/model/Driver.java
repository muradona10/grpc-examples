package com.md.formula1.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Driver")
@Table(name="f1_driver")
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "profile_photo_url")
    private String profilePhotoUrl;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "number", nullable = false)
    private Long number;

    @Column(name = "county_name")
    private String countryName;

    @Column(name = "county_flag_url")
    private String countryFlagUrl;

    @ManyToOne()
    @JoinColumn(name = "team_id")
    private Team team;

    public Driver(String profilePhotoUrl, String firstName, String lastName, Long number, String countryName, String countryFlagUrl, Team team) {
        this.profilePhotoUrl = profilePhotoUrl;
        this.firstName = firstName;
        this.lastName = lastName;
        this.number = number;
        this.countryName = countryName;
        this.countryFlagUrl = countryFlagUrl;
        this.team = team;
    }
}
