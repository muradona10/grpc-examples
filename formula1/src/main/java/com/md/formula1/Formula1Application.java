package com.md.formula1;

import com.md.formula1.model.Driver;
import com.md.formula1.model.Team;
import com.md.formula1.repository.DriverRepository;
import com.md.formula1.repository.TeamRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class Formula1Application implements CommandLineRunner {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private DriverRepository driverRepository;

    public static void main(String[] args) {
        SpringApplication.run(Formula1Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        log.info("To add sample data please open commented line below");
        //doSampleData();

    }

    public void doSampleData() {
        Team ferrari = new Team("Scuderia Ferrari Mission Winnow", "Italy", "Italy.png");
        Team mercedes = new Team("Mercedes-AMG Petronas F1 Team", "Germany", "Germany.png");
        Team haas = new Team("Haas F1 Team", "United States", "United_States.png");
        Team renault = new Team("Renault DP World F1 Team", "France", "France.png");
        Team redbull = new Team("Aston Martin Red Bull Racing", "United Kingdom", "United_Kingdom.png");
        teamRepository.save(ferrari);
        teamRepository.save(mercedes);
        teamRepository.save(haas);
        teamRepository.save(renault);
        teamRepository.save(redbull);

        Driver leclerc = new Driver("Leclerc.jpg", "Charles", "Leclerc", Long.valueOf(16), "Monaco", "Monaco.png", ferrari);
        Driver vettel = new Driver("Vettel.jpg", "Sebastian", "Vettel", Long.valueOf(5), "Germany", "Germany.png", ferrari);
        driverRepository.save(leclerc);
        driverRepository.save(vettel);

        Driver hamilton = new Driver("Hamilton.jpg", "Lewis", "Hamilton", Long.valueOf(44), "United Kingdom", "United_Kingdom.png", mercedes);
        Driver bottas = new Driver("Bottas.jpg", "Valtteri", "Bottas", Long.valueOf(77), "Finland", "Finland.png", mercedes);
        driverRepository.save(hamilton);
        driverRepository.save(bottas);

        Driver grosjean = new Driver("Grosjean.jpg", "Romain", "Grosjean", Long.valueOf(8), "France", "France.png", haas);
        Driver magnussen = new Driver("Magnussen.jpg", "Kevin", "Magnussen", Long.valueOf(20), "Denmark", "Denmark.png", haas);
        driverRepository.save(grosjean);
        driverRepository.save(magnussen);

        Driver ricciardo = new Driver("Ricciardo.jpg", "Daniel", "Ricciardo", Long.valueOf(3), "Australia", "Australia.png", renault);
        Driver ocon = new Driver("Ocon.jpg", "Esteban", "Ocon", new Long(31), "France", "France.png", renault);
        driverRepository.save(ricciardo);
        driverRepository.save(ocon);

        Driver verstappen = new Driver("Verstappen.jpg", "Max", "Verstappen", Long.valueOf(33), "Netherland", "Netherland.png", redbull);
        Driver albon = new Driver("Albon.jpg", "Alexander", "Albon", Long.valueOf(23), "Thailand", "Thailand.png", redbull);
        driverRepository.save(verstappen);
        driverRepository.save(albon);
    }
}
