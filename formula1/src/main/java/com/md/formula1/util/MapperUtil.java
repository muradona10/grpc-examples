package com.md.formula1.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MapperUtil {

    public static <T> List<T> fromIterableToList(Iterable<T> tIterable){
        List<T> list = new ArrayList<>();
        Iterator<T> it = tIterable.iterator();
        while (it.hasNext()){
            list.add(it.next());
        }
        return list;
    }

}
