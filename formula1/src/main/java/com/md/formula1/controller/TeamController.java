package com.md.formula1.controller;

import com.md.formula1.model.Driver;
import com.md.formula1.model.Team;
import com.md.formula1.repository.TeamRepository;
import com.md.formula1.util.FileUploadUtil;
import com.md.formula1.util.MapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/f1/teams")
public class TeamController {

    @Autowired
    private TeamRepository teamRepository;

    @GetMapping("/addTeam")
    public String getSaveTeamPage(Model model) {
        Team team = new Team();
        model.addAttribute("team", team);
        return "addF1Team";
    }

    @PostMapping("/team")
    public String saveTeam(Model model, Team team,
                           @RequestParam("teamCountyPhoto") MultipartFile teamCountyPhoto,
                           HttpServletRequest request) {

        String teamCountyUrl = FileUploadUtil.uploadFile(teamCountyPhoto, request);

        team.setCountryFlagUrl(teamCountyUrl);

        teamRepository.save(team);

        List<Team> teams = MapperUtil.fromIterableToList(teamRepository.findAll());
        model.addAttribute("teams", teams);

        return "teams";
    }


    @GetMapping("/list")
    public String teamList(Model model) {
        List<Team> teams = MapperUtil.fromIterableToList(teamRepository.findAll());
        model.addAttribute("teams", teams);

        return "teams";
    }

}
