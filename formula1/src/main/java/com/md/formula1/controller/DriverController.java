package com.md.formula1.controller;

import com.md.formula1.model.Driver;
import com.md.formula1.model.Team;
import com.md.formula1.repository.DriverRepository;
import com.md.formula1.repository.TeamRepository;
import com.md.formula1.util.FileUploadUtil;
import com.md.formula1.util.MapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/f1/drivers")
public class DriverController {

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private TeamRepository teamRepository;

    @GetMapping("/addDriver")
    public String getSaveDriverPage(Model model){
        Driver driver = new Driver();
        model.addAttribute("driver",driver);

        List<Team> teams = MapperUtil.fromIterableToList(teamRepository.findAll());
        model.addAttribute("teams",teams);
        return "addF1Driver";
    }

    @PostMapping("/driver")
    public String saveDriver(Model model, Driver driver,
                             @RequestParam("driverPhoto") MultipartFile driverPhoto,
                             @RequestParam("driverCountryPhoto") MultipartFile driverCountryPhoto,
                             HttpServletRequest request){

        String driverCountyUrl = FileUploadUtil.uploadFile(driverCountryPhoto, request);
        String driverPhotoUrl = FileUploadUtil.uploadFile(driverPhoto, request);

        driver.setCountryFlagUrl(driverCountyUrl);
        driver.setProfilePhotoUrl(driverPhotoUrl);

        Team team = teamRepository.findById(driver.getTeam().getId()).get();
        driver.setTeam(team);
        driverRepository.save(driver);

        List<Driver> drivers = MapperUtil.fromIterableToList(driverRepository.findAll());
        model.addAttribute("drivers", drivers);

        return "drivers";
    }


    @GetMapping("/list")
    public String driverList(Model model){
        List<Driver> drivers = MapperUtil.fromIterableToList(driverRepository.findAll());
        model.addAttribute("drivers", drivers);

        return "drivers";
    }


}
