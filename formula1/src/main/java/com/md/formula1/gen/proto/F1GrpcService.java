package com.md.formula1.gen.proto;

import com.md.formula1.mapper.F1GRPCMapper;
import com.md.formula1.model.Driver;
import com.md.formula1.model.Team;
import com.md.formula1.repository.DriverRepository;
import com.md.formula1.repository.TeamRepository;
import com.md.formula1.util.MapperUtil;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@GrpcService
public class F1GrpcService extends Formula1ServiceGrpc.Formula1ServiceImplBase {

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private TeamRepository teamRepository;

    public void getAllTeams(GetAllTeamsRequest request,
                            StreamObserver<GetAllTeamsResponse> responseStreamObserver){

        List<Team> teams = MapperUtil.fromIterableToList(teamRepository.findAll());

        List<F1Team> f1Teams = teams.stream()
                .map(F1GRPCMapper::mapToF1Team)
                .collect(Collectors.toList());

        GetAllTeamsResponse response = GetAllTeamsResponse.newBuilder()
                .addAllTeams(f1Teams)
                .build();

        responseStreamObserver.onNext(response);
        responseStreamObserver.onCompleted();

    }

    public void getAllDrivers(GetAllDriversRequest request,
                            StreamObserver<GetAllDriversResponse> responseStreamObserver){

        List<Driver> drivers = MapperUtil.fromIterableToList(driverRepository.findAll());

        List<F1Driver> f1Drivers = drivers.stream()
                .map(F1GRPCMapper::mapToF1Driver)
                .collect(Collectors.toList());

        GetAllDriversResponse response = GetAllDriversResponse.newBuilder()
                .addAllDrivers(f1Drivers)
                .build();

        responseStreamObserver.onNext(response);
        responseStreamObserver.onCompleted();

    }

}
