package com.md.grpc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.md.grpc.generated.Movie;

@Component
public class MovieRepository {

	private static final Map<Integer, Movie> movies = new HashMap<>();

	@PostConstruct
	public void initData() {
		
		Movie.Gender gender1 = Movie.Gender.newBuilder().setId(1).setName("Comedy").build();
		Movie.Gender gender2 = Movie.Gender.newBuilder().setId(2).setName("Action").build();
		Movie.Gender gender3 = Movie.Gender.newBuilder().setId(3).setName("Sci-Fi").build();
		List<Movie.Gender> genders = new ArrayList<>();
		genders.add(gender2);
		genders.add(gender3);

		Movie.Director director1 = Movie.Director.newBuilder().setId(1).setName("Director1").setImdb("sk1022609").build();
		Movie.Director director2 = Movie.Director.newBuilder().setId(2).setName("Director2").setImdb("sk1022610").build();
		List<Movie.Director> directors = new ArrayList<>();
		directors.add(director1);
		directors.add(director2);
		
		// movie1
		Movie movie1 = Movie.newBuilder().setId(1).setTitle("Thousands Words").setYear(2009).setImdb("tt1034064")
				.addGenders(gender1).addAllDirectors(directors).build();

		// movie2
		Movie movie2 = Movie.newBuilder().setId(2).setTitle("Gamer").setYear(2009).setImdb("tt1034032")
				.addAllGenders(genders).addDirectors(director1).build();

		movies.put(movie1.getId(), movie1);
		movies.put(movie2.getId(), movie2);

	}

	public List<Movie> getMoviesByDirectorName(String directorName) {

		List<Movie> moviesByDirecorName = new ArrayList<>();
		
		for (Movie m : movies.values()) {
			List<Movie.Director> directors = m.getDirectorsList();
			for (Movie.Director d : directors) {
				if (d.getName().equals(directorName)) {
					moviesByDirecorName.add(m);
				}
			}

		}

		return moviesByDirecorName;
	}
	
}
