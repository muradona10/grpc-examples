package com.md.grpc.client;

import com.md.grpc.generated.GetMoviesByDirectorNameGrpc;
import com.md.grpc.generated.GetMoviesByDirectorNameRequest;
import com.md.grpc.generated.GetMoviesByDirectorNameResponse;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class GrpcClient {

	public static void main(String [] args) {

		ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080).usePlaintext().build();
		
		GetMoviesByDirectorNameGrpc.GetMoviesByDirectorNameBlockingStub stub = GetMoviesByDirectorNameGrpc.newBlockingStub(channel);
		
		GetMoviesByDirectorNameResponse response = stub.getMoviesByDirectorName(GetMoviesByDirectorNameRequest
				                                       .newBuilder()
				                                       .setDirectorName("Director1")
				                                       .build());
		
		System.out.println("--------------------------------------------------------------------------------");
		System.out.println("-------------------------------    GRPC EXAMPLE   ------------------------------");
		System.out.println("--------------------------------------------------------------------------------");
		System.out.println("Movie Count:" + response.getMoviesCount());
		response.getMoviesList().forEach(movie -> {
			
			System.out.println("ID   : " + movie.getId());
			System.out.println("TITLE: " + movie.getTitle());
			System.out.println("YEAR : " + movie.getYear());
			System.out.println("IMDB : " + movie.getImdb());
			System.out.println("GENDERS:");
			
			movie.getGendersList().forEach(gender -> {
				System.out.println("ID   : " + gender.getId());
				System.out.println("NAME : " + gender.getName());
			});
			
			System.out.println("DIRECTORS:");
			movie.getDirectorsList().forEach(director -> {
				System.out.println("ID   : " + director.getId());
				System.out.println("NAME : " + director.getName());
				System.out.println("IMDB : " + director.getImdb());
			});
			
		});
		
		channel.shutdown();
	}
	
}
