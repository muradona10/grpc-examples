package com.md.grpc.server;

import java.io.IOException;

import com.md.grpc.GetMoviesByDirectorNameImpl;

import io.grpc.Server;
import io.grpc.ServerBuilder;

public class GrpcServer {
	
	public void connectServer() throws IOException, InterruptedException {
		
		Server server = ServerBuilder.forPort(8080).addService(new GetMoviesByDirectorNameImpl()).build();
		server.start();
		server.awaitTermination();
	}

}
