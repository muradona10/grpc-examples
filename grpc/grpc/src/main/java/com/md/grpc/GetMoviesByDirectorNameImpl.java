package com.md.grpc;

import java.util.List;

import com.md.grpc.generated.GetMoviesByDirectorNameRequest;
import com.md.grpc.generated.GetMoviesByDirectorNameResponse;
import com.md.grpc.generated.Movie;
import com.md.grpc.generated.GetMoviesByDirectorNameGrpc.GetMoviesByDirectorNameImplBase;

import io.grpc.stub.StreamObserver;

public class GetMoviesByDirectorNameImpl extends GetMoviesByDirectorNameImplBase {

	private MovieRepository movieRepository = new MovieRepository();
	
	@Override
	public void getMoviesByDirectorName(GetMoviesByDirectorNameRequest request, 
			                            StreamObserver<GetMoviesByDirectorNameResponse> responseObserver) {
		
		System.out.println("Request received from client:\n" + request);
		
		String directorName = request.getDirectorName();
		
		System.out.println("Call Repository:" + movieRepository.toString());
		List<Movie> movies = movieRepository.getMoviesByDirectorName(directorName);
		
		GetMoviesByDirectorNameResponse response = GetMoviesByDirectorNameResponse.newBuilder().addAllMovies(movies).build();
		
		responseObserver.onNext(response);
		responseObserver.onCompleted();
		
	}

}
