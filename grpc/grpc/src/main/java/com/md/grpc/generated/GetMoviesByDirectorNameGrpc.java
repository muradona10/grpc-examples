package com.md.grpc.generated;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.4.0)",
    comments = "Source: MovieLibrary.proto")
public final class GetMoviesByDirectorNameGrpc {

  private GetMoviesByDirectorNameGrpc() {}

  public static final String SERVICE_NAME = "com.md.grpc.GetMoviesByDirectorName";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.md.grpc.generated.GetMoviesByDirectorNameRequest,
      com.md.grpc.generated.GetMoviesByDirectorNameResponse> METHOD_GET_MOVIES_BY_DIRECTOR_NAME =
      io.grpc.MethodDescriptor.<com.md.grpc.generated.GetMoviesByDirectorNameRequest, com.md.grpc.generated.GetMoviesByDirectorNameResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "com.md.grpc.GetMoviesByDirectorName", "getMoviesByDirectorName"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.md.grpc.generated.GetMoviesByDirectorNameRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.md.grpc.generated.GetMoviesByDirectorNameResponse.getDefaultInstance()))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static GetMoviesByDirectorNameStub newStub(io.grpc.Channel channel) {
    return new GetMoviesByDirectorNameStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static GetMoviesByDirectorNameBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new GetMoviesByDirectorNameBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static GetMoviesByDirectorNameFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new GetMoviesByDirectorNameFutureStub(channel);
  }

  /**
   */
  public static abstract class GetMoviesByDirectorNameImplBase implements io.grpc.BindableService {

    /**
     */
    public void getMoviesByDirectorName(com.md.grpc.generated.GetMoviesByDirectorNameRequest request,
        io.grpc.stub.StreamObserver<com.md.grpc.generated.GetMoviesByDirectorNameResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_GET_MOVIES_BY_DIRECTOR_NAME, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_GET_MOVIES_BY_DIRECTOR_NAME,
            asyncUnaryCall(
              new MethodHandlers<
                com.md.grpc.generated.GetMoviesByDirectorNameRequest,
                com.md.grpc.generated.GetMoviesByDirectorNameResponse>(
                  this, METHODID_GET_MOVIES_BY_DIRECTOR_NAME)))
          .build();
    }
  }

  /**
   */
  public static final class GetMoviesByDirectorNameStub extends io.grpc.stub.AbstractStub<GetMoviesByDirectorNameStub> {
    private GetMoviesByDirectorNameStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GetMoviesByDirectorNameStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GetMoviesByDirectorNameStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GetMoviesByDirectorNameStub(channel, callOptions);
    }

    /**
     */
    public void getMoviesByDirectorName(com.md.grpc.generated.GetMoviesByDirectorNameRequest request,
        io.grpc.stub.StreamObserver<com.md.grpc.generated.GetMoviesByDirectorNameResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_GET_MOVIES_BY_DIRECTOR_NAME, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class GetMoviesByDirectorNameBlockingStub extends io.grpc.stub.AbstractStub<GetMoviesByDirectorNameBlockingStub> {
    private GetMoviesByDirectorNameBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GetMoviesByDirectorNameBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GetMoviesByDirectorNameBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GetMoviesByDirectorNameBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.md.grpc.generated.GetMoviesByDirectorNameResponse getMoviesByDirectorName(com.md.grpc.generated.GetMoviesByDirectorNameRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_GET_MOVIES_BY_DIRECTOR_NAME, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class GetMoviesByDirectorNameFutureStub extends io.grpc.stub.AbstractStub<GetMoviesByDirectorNameFutureStub> {
    private GetMoviesByDirectorNameFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GetMoviesByDirectorNameFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GetMoviesByDirectorNameFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GetMoviesByDirectorNameFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.md.grpc.generated.GetMoviesByDirectorNameResponse> getMoviesByDirectorName(
        com.md.grpc.generated.GetMoviesByDirectorNameRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_GET_MOVIES_BY_DIRECTOR_NAME, getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_MOVIES_BY_DIRECTOR_NAME = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final GetMoviesByDirectorNameImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(GetMoviesByDirectorNameImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_MOVIES_BY_DIRECTOR_NAME:
          serviceImpl.getMoviesByDirectorName((com.md.grpc.generated.GetMoviesByDirectorNameRequest) request,
              (io.grpc.stub.StreamObserver<com.md.grpc.generated.GetMoviesByDirectorNameResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class GetMoviesByDirectorNameDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.md.grpc.generated.MovieLibrary.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (GetMoviesByDirectorNameGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new GetMoviesByDirectorNameDescriptorSupplier())
              .addMethod(METHOD_GET_MOVIES_BY_DIRECTOR_NAME)
              .build();
        }
      }
    }
    return result;
  }
}
