package com.md.grpc;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.md.grpc.server.GrpcServer;


@SpringBootApplication
public class GrpcApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(GrpcApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		GrpcServer grpcServer = new GrpcServer();
		grpcServer.connectServer();
	}

}
