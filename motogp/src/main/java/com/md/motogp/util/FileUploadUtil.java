package com.md.motogp.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public class FileUploadUtil {
	
	private static final String UPLOADED_FILE_URL = System.getProperty("user.dir") + "\\uploads";
	
	public static String uploadFile(MultipartFile file, HttpServletRequest request) {
		new File(UPLOADED_FILE_URL).mkdir();
		
		Path path = Paths.get(UPLOADED_FILE_URL, file.getOriginalFilename());
		try {
			Files.write(path, file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}

		String url = String.format("%s://%s:%d/uploads/%s", request.getScheme()
				, request.getServerName(), request.getServerPort(), file.getOriginalFilename());

		return url;
	}

}
