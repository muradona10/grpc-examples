package com.md.motogp.restcontroller;

import com.md.motogp.model.Rider;
import com.md.motogp.model.RiderList;
import com.md.motogp.repository.RiderRepository;
import com.md.motogp.util.MapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/motogp/rest")
public class MotoGPController {

    @Autowired
    private RiderRepository riderRepository;

    @GetMapping("/riders")
    @ResponseBody
    public RiderList riders(){
        List<Rider> riders = MapperUtil.fromIterableToList(riderRepository.findAll());
        return new RiderList(riders);
    }

}
