package com.md.motogp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.md.motogp")
public class MotogpApplication {

	public static void main(String[] args) {
		SpringApplication.run(MotogpApplication.class, args);
	}

}
