package com.md.motogp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.md.motogp.model.Rider;
import com.md.motogp.repository.RiderRepository;
import com.md.motogp.util.FileUploadUtil;
import com.md.motogp.util.MapperUtil;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/motogp/riders")
public class RiderController {

    @Autowired
    private RiderRepository riderRepository;

    @GetMapping("/addRider")
    public String home(Model model){
    	Rider rider = new Rider();
    	model.addAttribute("rider", rider);
        return "addRider";
    }

    @PostMapping("/saveRider")
    public String addMotoGPRacer(Model model, Rider rider,
                                 @RequestParam("profilePhoto") MultipartFile profilePhoto,
                                 @RequestParam("countryPhoto") MultipartFile countryPhoto,
                                 HttpServletRequest request){
    	
    	String profilePhotoUrl = FileUploadUtil.uploadFile(profilePhoto, request);
    	rider.setProfilePhotoUrl(profilePhotoUrl);
    	String countryPhotoUrl = FileUploadUtil.uploadFile(countryPhoto, request);
    	rider.setCountryPhotoUrl(countryPhotoUrl);
    	riderRepository.save(rider);
    	
    	List<Rider> riders = MapperUtil.fromIterableToList(riderRepository.findAll());
    	model.addAttribute("riders", riders);
        return "riders";
    }
    
    @GetMapping("/list")
    public String listOfRiders(Model model){
    	List<Rider> riders = MapperUtil.fromIterableToList(riderRepository.findAll());
    	model.addAttribute("riders", riders);
        return "riders";
    }


}
