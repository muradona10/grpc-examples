package com.md.motogp.repository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.md.motogp.model.Rider;

@Slf4j
@Component
public class DBLoader implements CommandLineRunner{
	
	private RiderRepository riderRepository;
	
	@Autowired
	public DBLoader(RiderRepository riderRepository) {
		this.riderRepository = riderRepository;
	}


	@Override
	public void run(String... args) throws Exception {

		log.info("Open commented lines to save dummy data in db");

		/*
		Rider rossi = new Rider("Valentino", "Rossi", "Rossi.jpg", "Italy.png", "Monster Energy Yamaha MotoGP", new Long(46));
		Rider vinales = new Rider("Maveric", "Vinales", "Vinales.jpg", "Spain.png",  "Monster Energy Yamaha MotoGP", new Long(12));
		Rider dovizioso = new Rider("Andrea", "Dovizioso", "Dovizioso.jpg", "Italy.png",  "Ducati Team", new Long(4));
		Rider iannone = new Rider("Andrea", "Iannone", "Iannone.jpg", "Italy.png",  "Aprilia Racing Team Gresini", new Long(29));
		Rider oliveira = new Rider("Miguel", "Oliveira", "Oliveira.jpg", "Portugal.png",  "Red Bull KTM Tech 3", new Long(88));
		Rider quartararo = new Rider("Fabio", "Quartararo", "Quartararo.jpg", "France.png",  "Petronas Yamaha SRT", new Long(20));
		
		riderRepository.save(rossi);
		riderRepository.save(vinales);
		riderRepository.save(dovizioso);
		riderRepository.save(iannone);
		riderRepository.save(oliveira);
		riderRepository.save(quartararo);
		*/
		
	}

}
