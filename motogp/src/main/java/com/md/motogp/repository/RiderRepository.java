package com.md.motogp.repository;

import com.md.motogp.model.Rider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RiderRepository extends CrudRepository<Rider, Long> {
}
