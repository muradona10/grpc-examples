package com.md.motogp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="motogp_rider")
public class Rider {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ID;

    @Column(name="name", nullable = false)
    private String name;

    @Column(name="surname", nullable = false)
    private String surname;

    @Column(name="profile_photo_url")
    private String profilePhotoUrl;
    
    @Column(name="country_photo_url")
    private String countryPhotoUrl;

    @Column(name="team_name", nullable = false)
    private String teamName;
    
    @Column(name="number")
    private Long number;
    
    public Rider() {
	}

	public Rider(String name, String surname, String profilePhotoUrl, String countryPhotoUrl, String teamName,
			Long number) {
		super();
		this.name = name;
		this.surname = surname;
		this.profilePhotoUrl = profilePhotoUrl;
		this.countryPhotoUrl = countryPhotoUrl;
		this.teamName = teamName;
		this.number = number;
	}

}
