package com.md.motogp.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class RiderList {

    private List<Rider> riders;

}
